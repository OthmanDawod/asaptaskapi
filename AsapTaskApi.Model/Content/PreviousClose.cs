﻿namespace AsapTaskApi.Model.Content
{
    public class PreviousClose
    {
        public int Id { get; set; }
        public string Ticker { get; set; }
        public float TradingVolume { get; set; }
        public float VolumeWeighted { get; set; }
        public float OpenPrice { get; set; }
        public float ClosePrice { get; set; }
        public float HighestPrice { get; set; }
        public float LowestPrice { get; set; }
        public float UnixMsecTimestamp { get; set; }
        public float TransactionsNumber { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}
