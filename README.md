# ASAP Task Project

This document provides an overview of the ASAP Task Project API

Features:
In this project, we are adding Clients in addition to communicating with https://polygon.io API to get specific data about
market previous close prices.

The API configuration can be found in the appsettings.json file. 
Inorder to run the project you have to modify:

- "ConnectionStrings": Create MSSQL database and edit the "ConnectionStrings" with the database you created.

- "EmailConfiguration": Insert your own Email information "EmailConfiguration" to run the service related to sending emails.

- "ApiKey": Insert Polygon api key.

For any questions or feedback, feel free to contact with me.

