﻿using AsapTaskApi.Model.Content;


namespace AsapTaskApi.BoundedContext.Content.IData
{
    public interface IPreviousCloseRepository
    {
        PreviousClose? Create(PreviousClose previousClose);

        PreviousClose? GetLatest();
    }
}
