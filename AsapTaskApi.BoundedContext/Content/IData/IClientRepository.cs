﻿using AsapTaskApi.BoundedContext.Content.Dto;
using AsapTaskApi.BoundedContext.Filter;
using AsapTaskApi.Model.Content;

namespace AsapTaskApi.BoundedContext.Content.IData
{
    public interface IClientRepository
    {
        List<ClientDto> GetAll(PaginationFilter filter, string? clientFirstName = null, string? clientLastName = null, string? clientEmail = null, string? clientPhoneNumber = null);

        List<string> GetEmails();

        ClientDto? GetById(int id);

        Client? Update(ClientDto clientDto);

        Client? Create(ClientDto clientDto);

        bool Remove(int id);
    }
}
