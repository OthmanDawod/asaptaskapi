﻿using System.ComponentModel.DataAnnotations;

namespace AsapTaskApi.BoundedContext.Content.Dto
{
    public class ClientDto
    {
        public int Id { get; set; }

        [MaxLength(150)]
        public string FirstName { get; set; }

        [MaxLength(150)]
        public string LastName { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string PhoneNumber { get; set; }
    }
}
