﻿using AsapTaskApi.BoundedContext.Content.Dto;
using AsapTaskApi.BoundedContext.Content.IData;
using AsapTaskApi.BoundedContext.Filter;
using AsapTaskApi.Model.Content;
using AsapTaskApi.SqlServer.DataBase;

namespace AsapTaskApi.BoundedContext.Content.Data
{
    public class ClientRepository : IClientRepository
    {
        private readonly AsapTaskDbContext Context;

        public ClientRepository(AsapTaskDbContext context)
        {
            Context = context;
        }

        public Client? Create(ClientDto clientDto)
        {
            try
            {
                var clientEntity = Context.Client.Where(cl => String.Equals(cl.Email, clientDto.Email)).SingleOrDefault();
                if (clientEntity != null)
                {
                    return null;
                }

                clientEntity = new Client()
                {
                    FirstName = clientDto.FirstName,
                    LastName = clientDto.LastName,
                    Email = clientDto.Email,
                    PhoneNumber = clientDto.PhoneNumber,
                };
                Context.Client.Add(clientEntity);
                Context.SaveChanges();

                return clientEntity;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ClientDto? GetById(int id)
        {
            return Context.Client
                .Where(cl => cl.Id == id)
                .Select(cl => new ClientDto()
                {
                    Id = cl.Id,
                    FirstName = cl.FirstName,
                    LastName = cl.LastName,
                    Email = cl.Email,
                    PhoneNumber = cl.PhoneNumber,
                })
                .SingleOrDefault();
        }

        public List<ClientDto> GetAll(PaginationFilter filter, string? clientFirstName = null, string? clientLastName = null, string? clientEmail = null, string? clientPhoneNumber = null)
        {
            var query = Context.Client.AsQueryable();

            if (!String.IsNullOrWhiteSpace(clientFirstName))
            {
                query = query.Where(cl => cl.FirstName.Contains(clientFirstName));
            }

            if (!String.IsNullOrWhiteSpace(clientLastName))
            {
                query = query.Where(cl => cl.LastName.Contains(clientLastName));
            }

            if (!String.IsNullOrWhiteSpace(clientEmail))
            {
                query = query.Where(cl => cl.Email.Contains(clientEmail));
            }

            if (!String.IsNullOrWhiteSpace(clientPhoneNumber))
            {
                query = query.Where(cl => cl.PhoneNumber.Contains(clientPhoneNumber));
            }

            return query
                .OrderByDescending(cl => cl.Id)
                .Select(cl => new ClientDto()
                {
                    Id = cl.Id,
                    FirstName = cl.FirstName,
                    LastName = cl.LastName,
                    Email = cl.Email,
                    PhoneNumber = cl.PhoneNumber,
                })
                .Skip((filter.PageNumber - 1) * filter.PageSize)
                .Take(filter.PageSize)
                .ToList();
        }

        public List<string> GetEmails()
        {
            return Context.Client
                .Select(cl => cl.Email)
                .ToList();
        }

        public bool Remove(int id)
        {
            try
            {
                var clientEntity = Context.Client.Find(id);
                if (clientEntity == null)
                {
                    return false;
                }
                Context.Client.Remove(clientEntity);
                Context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Client? Update(ClientDto clientDto)
        {
            try
            {
                var clientEntity = Context.Client.Where(cc => cc.Id == clientDto.Id).SingleOrDefault();
                if (clientEntity == null)
                {
                    return null;
                }

                clientEntity.FirstName = clientDto.FirstName;
                clientEntity.LastName = clientDto.LastName;
                clientEntity.Email = clientDto.Email;
                clientEntity.PhoneNumber = clientDto.PhoneNumber;
                Context.Client.Update(clientEntity);
                Context.SaveChanges();

                return clientEntity;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
