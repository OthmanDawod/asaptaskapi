﻿using AsapTaskApi.BoundedContext.Content.IData;
using AsapTaskApi.Model.Content;
using AsapTaskApi.SqlServer.DataBase;

namespace AsapTaskApi.BoundedContext.Content.Data
{
    public class PreviousCloseRepository : IPreviousCloseRepository
    {
        private readonly AsapTaskDbContext Context;

        public PreviousCloseRepository(AsapTaskDbContext context)
        {
            Context = context;
        }

        public PreviousClose? Create(PreviousClose previousClose)
        {
            try
            {
                if (previousClose.Ticker != null)
                {
                    Context.PreviousClose.Add(previousClose);
                    Context.SaveChanges();
                    return previousClose;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public PreviousClose? GetLatest()
        {
            return Context.PreviousClose
                .OrderByDescending(x => x.CreatedAt)
                .FirstOrDefault();
        }
    }
}
