﻿using System.ComponentModel.DataAnnotations;

namespace AsapTaskApi.ViewModels.Client
{
    public class ClientViewModelC
    {
        [Required]
        [MaxLength(150)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(150)]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string PhoneNumber { get; set; }
    }
}
