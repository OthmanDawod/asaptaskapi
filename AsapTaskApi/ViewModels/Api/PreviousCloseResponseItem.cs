﻿namespace AsapTaskApi.ViewModels.Api
{
    public class PreviousCloseResponseItem
    {
        public string T;
        public float v;
        public float vw;
        public float o;
        public float c;
        public float h;
        public float l;
        public float t;
        public float n;
    }
}
