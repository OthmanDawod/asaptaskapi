﻿namespace AsapTaskApi.ViewModels.Api
{
    public class PreviousCloseResponse
    {
        public bool Adjusted { get; set; }
        public int QueryCount { get; set; }
        public string Request_Id { get; set; }
        public List<PreviousCloseResponseItem> results { get; set; }
        public int ResultsCount { get; set; }
        public string Status { get; set; }
        public string Ticker { get; set; }
    }
}
