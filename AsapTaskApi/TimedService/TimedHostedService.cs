﻿using AsapTaskApi.ApiService;
using AsapTaskApi.BoundedContext.Content.IData;
using AsapTaskApi.EmailService;
using AsapTaskApi.Model.Content;
using AsapTaskApi.ViewModels.Api;
namespace AsapTaskApi.TimedService
{
    public class TimedHostedService : IHostedService, IDisposable
    {
        private readonly ILogger<TimedHostedService> _logger;
        private Api _apiService;
        private IConfiguration _config { get; }
        private readonly IServiceScopeFactory _scopeFactory;
        private Timer? _timer = null;

        public TimedHostedService(ILogger<TimedHostedService> logger, IConfiguration config, IServiceScopeFactory scopeFactory)
        {
            _logger = logger;
            _config = config;
            _apiService = new Api(_config["polygon:ApiKey"]);
            _scopeFactory = scopeFactory;
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Timed Hosted Service running.");
            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromHours(6));

            return Task.CompletedTask;
        }

        private async void DoWork(object? state)
        {
            var response = await _apiService.GetPreviousClose("AAPL");
            InsertToDataBase(response);
            SendEmails();
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Timed Hosted Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

        public void InsertToDataBase(PreviousCloseResponse response)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var requiredRepository = scope.ServiceProvider.GetRequiredService<IPreviousCloseRepository>();
                foreach (var item in response.results)
                {
                    var result = new PreviousClose()
                    {
                        Ticker = item.T,
                        ClosePrice = item.c,
                        HighestPrice = item.h,
                        LowestPrice = item.l,
                        OpenPrice = item.o,
                        TradingVolume = item.v,
                        TransactionsNumber = item.n,
                        UnixMsecTimestamp = item.t,
                        VolumeWeighted = item.vw,
                    };
                    requiredRepository.Create(result);
                }
            }

        }
        public void SendEmails()
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var clientRepository = scope.ServiceProvider.GetRequiredService<IClientRepository>();
                var emailSenderRepository = scope.ServiceProvider.GetRequiredService<IEmailSender>();
                var previousCloseRepository = scope.ServiceProvider.GetRequiredService<IPreviousCloseRepository>();

                var clientsEmails = clientRepository.GetEmails();
                var lastData = previousCloseRepository.GetLatest();
                var message = new Message(clientsEmails, "Previous Close", GetEmailContent(lastData));
                emailSenderRepository.SendEmail(message);
            }

        }
        private string GetEmailContent(PreviousClose data)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var renderInterface = scope.ServiceProvider.GetRequiredService<IViewRenderService>();
                return renderInterface.RenderToStringAsync("email", data).Result;
            }
        }
    }
}
