﻿using AsapTaskApi.ViewModels.Api;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;

namespace AsapTaskApi.ApiService
{
    public class Api
    {
        private readonly string Base_URL = "https://api.polygon.io/v2/";
        private readonly HttpClient _httpClient;
        private string _apiKey;
        public Api(string apiKey)
        {
            _httpClient = new HttpClient()
            {
                BaseAddress = new Uri(Base_URL),
            };
            _apiKey = apiKey;
        }

        public async Task<PreviousCloseResponse> GetPreviousClose(string ticker)
        {
            var Queryparams = new Dictionary<string, string>();
            Queryparams.Add("adjusted", "true");
            Queryparams.Add("apiKey", _apiKey);
            var newUrl = new Uri(QueryHelpers.AddQueryString(Base_URL + "aggs/ticker/" + ticker + "/prev", Queryparams));
            try
            {
                var response = await _httpClient.GetAsync(newUrl);
                response.EnsureSuccessStatusCode();
                var stringData = await response.Content.ReadAsStringAsync();
                PreviousCloseResponse data = JsonConvert.DeserializeObject<PreviousCloseResponse>(stringData);
                return data;

            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"Error: {e.Message}");
                return null;
            }
            //try
            //{
            //    var response = await _httpClient.GetFromJsonAsync<PreviousCloseResponse>(newUrl);
            //    return response;

            //}
            //catch (Exception ex)
            //{

            //    throw;
            //}


            //return response;

            //if (response.IsSuccessStatusCode)
            //{
            //    string result = response.Content.ReadAsStringAsync().Result;
            //}

        }

    }
}
