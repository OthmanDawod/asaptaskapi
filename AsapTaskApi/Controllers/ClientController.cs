﻿using AsapTaskApi.BoundedContext.Content.Dto;
using AsapTaskApi.BoundedContext.Content.IData;
using AsapTaskApi.BoundedContext.Filter;
using AsapTaskApi.ViewModels.Client;
using Microsoft.AspNetCore.Mvc;

namespace AsapTaskApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : Controller
    {
        private IClientRepository ClientRepository { get; }

        public ClientController(IClientRepository clientRepository)
        {
            ClientRepository = clientRepository;
        }

        [HttpGet]
        public ActionResult<ClientDto> GetAll([FromQuery] PaginationFilter filter, string? firstName, string? lastName,
            string? email, string? phoneNumber)
        {
            var clients = ClientRepository.GetAll(filter, firstName, lastName, email, phoneNumber);

            return new JsonResult(clients);
        }

        [HttpPost]
        public IActionResult Create(ClientViewModelC vm)
        {
            var clientDto = new ClientDto()
            {
                FirstName = vm.FirstName,
                LastName = vm.LastName,
                Email = vm.Email,
                PhoneNumber = vm.PhoneNumber,
            };
            var result = ClientRepository.Create(clientDto);
            if (result == null)
            {
                return BadRequest("Create Clinet Failed!... Check Email or try again");
            }
            return new JsonResult(result);

        }

        [HttpPut("{id}")]
        public IActionResult UpdateClient(int id, ClientViewModelC vm)
        {
            var clientDto = new ClientDto()
            {
                Id = id,
                FirstName = vm.FirstName,
                LastName = vm.LastName,
                Email = vm.Email,
                PhoneNumber = vm.PhoneNumber,
            };
            var result = ClientRepository.Update(clientDto);
            if (result == null)
            {
                return BadRequest("Client Update Failed!..");
            }
            return new JsonResult(result);
        }

        [HttpGet("{id}")]
        public IActionResult GetClientById(int id)
        {
            var client = ClientRepository.GetById(id);
            var vm = new ClientViewModel()
            {
                Id = id,
                FirstName = client.FirstName,
                LastName = client.LastName,
                Email = client.Email,
                PhoneNumber = client.PhoneNumber,
            };
            return new JsonResult(vm);
        }

        [HttpDelete("{id}")]
        public IActionResult Remove(int id)
        {
            var clientdto = ClientRepository.GetById(id);
            if (clientdto == null)
            {
                return this.NotFound();
            }
            var client = ClientRepository.Remove(id);
            return new JsonResult(client);
        }
    }
}
