﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AsapTaskApi.SqlServer.Migrations
{
    /// <inheritdoc />
    public partial class PreviousClose : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PreviousClose",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ticker = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TradingVolume = table.Column<float>(type: "real", nullable: false),
                    VolumeWeighted = table.Column<float>(type: "real", nullable: false),
                    OpenPrice = table.Column<float>(type: "real", nullable: false),
                    ClosePrice = table.Column<float>(type: "real", nullable: false),
                    HighestPrice = table.Column<float>(type: "real", nullable: false),
                    LowestPrice = table.Column<float>(type: "real", nullable: false),
                    UnixMsecTimestamp = table.Column<float>(type: "real", nullable: false),
                    TransactionsNumber = table.Column<float>(type: "real", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PreviousClose", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PreviousClose");
        }
    }
}
