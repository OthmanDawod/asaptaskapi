﻿using AsapTaskApi.Model.Content;
using Microsoft.EntityFrameworkCore;

namespace AsapTaskApi.SqlServer.DataBase
{
    public class AsapTaskDbContext : DbContext
    {
        public AsapTaskDbContext(DbContextOptions<AsapTaskDbContext> options) : base(options) { }
        public DbSet<Client> Client { get; set; }
        public DbSet<PreviousClose> PreviousClose { get; set; }

    }
}
